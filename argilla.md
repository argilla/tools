# argilla
```
./argilla [ arg1 arg2 ... argN ]
```

# Env Variables
Put them in ./env, it will be loaded by argilla script.

* ```BACKUP_PATH``` path to directory for backups
* ```REPO_DOCKER_PATH``` path to docker scripts repository

# Usage
```sudo ./argilla project $name backup```

